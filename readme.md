
# EShopBD

E-commerce Site for eshopbd using magento.
### How to install
1.Clone the repo

2.Suppose your main directory is eshopbd. Run this command

    $ chmod 777 eshopbd
    $ cd eshopbd
    $ ./permission.sh
    $ cp app/etc/local.example.xml app/etc/local.xml
3.Create a Database and update the configuration on local.xml. During this time don't visit your site.
4.Now go to nbase folder and import this database. After then visit EShopBD site.

